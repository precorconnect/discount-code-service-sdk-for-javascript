/**
 * @class {DiscountCodeSynopsisView}
 */
export default class DiscountCodeSynopsisView {

    _id:number;

    _code:string;

    _type:string;

    _value:number;

    _maxValue:number;

    _description:string;

    _expirationDate:string;

    /**
     * @param {number} id
     * @param {string} code
     * @param {string} type
     * @param {number} value
     * @param {number} maxValue
     * @param {string} description
     * @param {string} expirationDate
     */
    constructor(id:number,
                code:string,
                type:string,
                value:number,
                maxValue:number,
                description:string,
                expirationDate:string) {

        if (!id) {
            throw new TypeError('id required');
        }
        this._id = id;

        if (!code) {
            throw new TypeError('code required');
        }
        this._code = code;

        if (!type) {
            throw new TypeError('type required');
        }
        this._type = type;

        if (!value) {
            throw new TypeError('value required');
        }
        this._value = value;

        this._maxValue = maxValue;

        if (!description) {
            throw new TypeError('description required');
        }
        this._description = description;

        if (!expirationDate) {
            throw new TypeError('expirationDate required');
        }
        this._expirationDate = expirationDate;

    }

    /**
     * @returns {number}
     */
    get id():number {
        return this._id;
    }

    /**
     * @returns {string}
     */
    get code():string {
        return this._code;
    }

    /**
     * @returns {string}
     */
    get type():string {
        return this._type;
    }

    /**
     * @returns {number}
     */
    get value():string {
        return this._value;
    }

    /**
     * @returns {number}
     */
    get maxValue():string {
        return this._maxValue;
    }

    /**
     * @returns {string}
     */
    get description():string {
        return this._description;
    }

    /**
     * @returns {string}
     */
    get expirationDate():string {
        return this._expirationDate;
    }

}