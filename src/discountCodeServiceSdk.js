import DiscountCodeServiceSdkConfig from './discountCodeServiceSdkConfig';
import DiContainer from './diContainer';
import DiscountCodeSynopsisView from './discountCodeSynopsisView';
import GetDiscountCodesFeature from './getDiscountCodesFeature';

/**
 * @class {DiscountCodeServiceSdk}
 */
export default class DiscountCodeServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {DiscountCodeServiceSdkConfig} config
     */
    constructor(config:DiscountCodeServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    /**
     * @param {string} accessToken
     * @returns {Array<CustomerSourceSynopsisView>}
     */
    getDiscountCode(code:string,accessToken:string):DiscountCodeSynopsisView {

        return this
            ._diContainer
            .get(GetDiscountCodesFeature)
            .execute(code,accessToken);

    }

}
