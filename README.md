## Description
Precor Connect discount code service SDK for javascript.

## Features

##### List Customer Sources
* [documentation](features/GetDiscountCode.feature)

## Setup

**install via jspm**  
```shell
jspm install discount-code-service-sdk=bitbucket:precorconnect/discount-code-service-sdk-for-javascript
``` 

**import & instantiate**
```javascript
import DiscountCodeServiceSdk,{DiscountCodeServiceSdkConfig} from 'discount-code-service-sdk'

const discountCodeServiceSdkConfig =
    new DiscountCodeServiceSdkConfig(
        "https://api-dev.precorconnect.com"
    );
    
const discountCodeServiceSdk =
    new DiscountCodeServiceSdk(
        discountCodeServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```