import DiscountCodeServiceSdk from '../../src/index';
import config from './config';
import factory from './factory';
import dummy from '../dummy';

describe('Index module', () => {

    describe('default export', () => {
        it('should be DiscountCodeServiceSdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new DiscountCodeServiceSdk(config.discountCodeServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(DiscountCodeServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('getDiscountCode method', () => {
            it('should return more than 1 result', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new DiscountCodeServiceSdk(config.discountCodeServiceSdkConfig);


                /*
                 act
                 */
                const discountCodePromise =
                    objectUnderTest
                        .getDiscountCode(
                            dummy.code,
                            factory
                                .constructValidPartnerRepOAuth2AccessToken()
                        );

                /*
                 assert
                 */
                discountCodePromise
                    .then((DiscountCodeSynopsisView) => {
                        expect(DiscountCodeSynopsisView).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            },20000)
        });
    });
});